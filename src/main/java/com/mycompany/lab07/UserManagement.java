/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab07;

import java.util.ArrayList;

/**
 *
 * @author Anony
 */
public class UserManagement {
    public static void main(String[] args) {
        User admin = new User("admin","Administrator","Pass@1234",'M','A');
        User user1 = new User("user","manan","Pass@1234",'M','U');
        User user2 = new User("user","mesayun","Pass@1234",'M','U');
        User user3 = new User("user","malaya","Pass@1234",'M','U');
        System.out.println(admin);
        System.out.println(user1);
        System.out.println(user2);
        
        User[] userArr = new User[3];
        userArr[0] = admin;
        userArr[1] = user1;
        userArr[2] = user2;
        
        System.out.println("PrintUserArr");
        for(int i=0;i<userArr.length;i++){
            System.out.println(userArr[i]);
        }
        
        System.out.println("PrintUserList");
        ArrayList<User> userList = new ArrayList();
        userList.add(admin);
        System.out.println(userList.get(0)+"List size = "+userList.size());
        userList.add(user1);
        System.out.println(userList.get(1)+"List size = "+userList.size());
        userList.add(user2);
        System.out.println(userList.get(2)+"List size = " + userList.size());
        userList.add(user3);
        System.out.println(userList.get(3)+"List size = " + userList.size());
        
        System.out.println("PrintForLoopUserList");
        for(int i =0 ; i<userList.size();i++){
            System.out.println(userList.get(i));
        }
        
        User user4 = new User("user 4","malaya","Pass@1234",'M','U');
        userList.set(1,user4);
        
        for( User u: userList){
            System.out.println(u);
        }
        
        System.out.println("Remove user");
        
        userList.remove(1);
        
        for( User u: userList){
            System.out.println(u);
        }
        
        
        
    }
    
    
}
